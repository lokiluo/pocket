angular.module('pocketApp.controllers', [])

.controller('SignInCtrl', function($scope, $state) {

	$scope.signIn = function(user) {
		console.log('Sign-In', user);
		if(user.username !== "loki"){
			user.loginFailed=true;
		} else {
		 	$state.go('tabs.home');
		}	
	};

})

.controller('HomeTabCtrl', function($scope) {
	console.log('HomeTabCtrl');
});