// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('pocketApp', ['ionic', 'pocketApp.controllers'])

.config(function($stateProvider, $urlRouterProvider) {
		$stateProvider
			.state('signin', {
				url: "/sign-in",
				templateUrl: "views/sign-in.html",
				controller: 'SignInCtrl'
			})
			.state('tabs', {
				url: "/tab",
				abstract: true,
				templateUrl: "views/tabs.html"
			})
			.state('tabs.home', {
				url: "home",
				views: {
					'home-tab': {
						templateUrl: "views/home.html",
						controller: 'HomeTabCtrl'
					}
				}
			})
			.state('tabs.about', {
				url: "about",
				views: {
					'about-tab': {
						templateUrl: "views/about.html"
					}
				}
		    })
		;


		$urlRouterProvider.otherwise("/sign-in")
	}
)



.run(function($ionicPlatform) {
	$ionicPlatform.ready(function() {
		if (window.StatusBar) {
			// Set the statusbar to use the default style, tweak this to
			// remove the status bar on iOS or change it to use white instead of dark colors.
			StatusBar.styleDefault();
		}
	});
})